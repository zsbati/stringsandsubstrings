

def count_substring(string, sub_string):
    if string.find(sub_string) == -1:
        return 0
    else:
        index = 0
        count = 0
        while string.find(sub_string) != -1:
            index = string.find(sub_string)
            string = string[index+1:] 
            count += 1
            if count > len(string):
                break
        return count

if __name__ == '__main__':
    string = input().strip()
    sub_string = input().strip()
    
    count = count_substring(string, sub_string)
    print(count)
